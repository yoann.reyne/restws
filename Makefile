compile :
	(cd HotelREST ; mvn clean install -DskipTests)
	(cd HotelREST2 ; mvn clean install -DskipTests)
	(cd HotelREST3 ; mvn clean install -DskipTests)
	(cd AgenceREST ; mvn clean install -DskipTests)
	(cd AgenceREST2 ; mvn clean install -DskipTests)
	(cd TrivagoREST ; mvn clean install -DskipTests)
	(cd UtilisateurREST ; mvn clean install -DskipTests)

pHotel1 :
	(cd HotelREST ; mvn spring-boot:run)

pHotel2 :
	(cd HotelREST2 ; mvn spring-boot:run)

pHotel3 :
	(cd HotelREST3 ; mvn spring-boot:run)

pAgence1 :
	(cd AgenceREST ; mvn spring-boot:run)

pAgence2 :
	(cd AgenceREST2 ; mvn spring-boot:run)

trivago :
	(cd TrivagoREST ; mvn exec:java@InterfaceGUI)

utilisateur :
	(cd UtilisateurREST ; mvn exec:java@InterfaceGUI)
