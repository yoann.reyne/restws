package com.hotel.data;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hotel.models.AgencePartenaire;
import com.hotel.models.Offre;
import com.hotel.repositories.AgencePartenaireRepository;
import com.hotel.repositories.OffreRepository;


@Configuration
public class HotelData {

	private Logger logger = LoggerFactory.getLogger(HotelData.class);
	DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			
	
	@Bean
	public CommandLineRunner initDatabaseOffre(OffreRepository repository) {
		return args -> {
			logger.info("J'enregistre une chambre : " + repository.save(
					new Offre(75, 3, "ChambreNice1000", df.parse("10-10-2022"), "../HotelREST2/images/room3.jpg")
					));
			logger.info("J'enregistre une chambre : " + repository.save(
					new Offre(110, 4, "ChambreNice1001", df.parse("19-10-2022"), "../HotelREST2/images/room4.jpg")
					));
			logger.info("J'enregistre une chambre : " + repository.save(
					new Offre(180, 2, "ChambreNice1002", df.parse("20-10-2022"), "../HotelREST2/images/room5.jpg")
					));
		};
	}
	
	
	@Bean
	public CommandLineRunner initDatabaseAgence(AgencePartenaireRepository repository) {
		return args -> {
			logger.info("J'enregistre une agence non partenaire : " + repository.save(
					new AgencePartenaire("AgenceNonPartenaire", 0.00)
					));
			logger.info("J'enregistre une agence partenaire : " + repository.save(
					new AgencePartenaire("ReserveTonHotel.com", 0.20)
					));
			logger.info("J'enregistre une agence partenaire : " + repository.save(
					new AgencePartenaire("ResaNord", 0.10)
					));
		};
	}
	
	
	
}
