package com.hotel.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hotel.models.Reservation;

public interface ReservationRepository extends JpaRepository<Reservation, Long>{
}
