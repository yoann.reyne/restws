package com.hotel.exceptions;

import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
public class ReservationDejaExistante extends Exception {

	public ReservationDejaExistante() {}
	
	public ReservationDejaExistante(String s) {
		super(s);
	}
	
}
