package com.hotel.models;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Reservation {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "idAgence")
	private long IdAgence;

	@Column(name = "idOffre")
	private long IdOffre;

	@Column(name = "idInfosClient")
	private long IdInfosClient;

	@Column(name = "dateDebut")
	private Date dateDebut;
	
	@Column(name = "dateFin")
	private Date dateFin;
	
	@Column(name = "prixResa")
	private double prix;
	
	public Reservation() {}
	
	public Reservation(long IdAgence, long IdOffre, long IdInfosClient,  Date dateDebut, Date dateFin, double prix){
		setIdAgence(IdAgence);
		setIdOffre(IdOffre);
		setIdInfosClient(IdInfosClient);
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.prix = prix;
	}
	
	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getIdAgence() {
		return IdAgence;
	}

	public void setIdAgence(long idAgence) {
		IdAgence = idAgence;
	}

	public long getIdOffre() {
		return IdOffre;
	}

	public void setIdOffre(long idOffre) {
		IdOffre = idOffre;
	}

	public long getIdInfosClient() {
		return IdInfosClient;
	}

	public void setIdInfosClient(long idInfosClient) {
		IdInfosClient = idInfosClient;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

}