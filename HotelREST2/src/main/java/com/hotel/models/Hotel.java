package com.hotel.models;

public class Hotel {
	
	private String pays;
	
	private String ville;

	private String rue;
	
	private String refHotel;
	
	private int etoiles;
	
	private String nomHotel;
	
	public Hotel() {}
	
	public Hotel(String pays, String ville, String rue, String refHotel, String nomHotel, int etoiles) {
		super();
		this.pays = pays;
		this.ville = ville;
		this.rue = rue;
		this.refHotel = refHotel;
		this.nomHotel = nomHotel;
		this.etoiles = etoiles;
	}

	

	public String getNomHotel() {
		return nomHotel;
	}

	public void setNomHotel(String nomHotel) {
		this.nomHotel = nomHotel;
	}

	public String getRefHotel() {
		return refHotel;
	}


	public void setRefHotel(String refHotel) {
		this.refHotel = refHotel;
	}
	
	public String getPays() {
		return pays;
	}

	
	public void setPays(String pays) {
		this.pays = pays;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	

	public int getEtoiles() {
		return etoiles;
	}

	public void setEtoiles(int etoiles) {
		this.etoiles = etoiles;
	}

}
