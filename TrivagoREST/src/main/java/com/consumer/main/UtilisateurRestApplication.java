package com.consumer.main;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class UtilisateurRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(UtilisateurRestApplication.class, args);
	}

}
