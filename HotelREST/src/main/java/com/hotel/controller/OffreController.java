package com.hotel.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hotel.exceptions.OffreNotFoundException;
import com.hotel.exceptions.ReservationDejaExistante;
import com.hotel.models.AgencePartenaire;
import com.hotel.models.ConsultationDTO;
import com.hotel.models.ConsultationTrivagoDTO;
import com.hotel.models.Hotel;
import com.hotel.models.InfosPersonnes;
import com.hotel.models.Offre;
import com.hotel.models.OffreDTO;
import com.hotel.models.Reservation;
import com.hotel.models.ReservationDTO;
import com.hotel.repositories.AgencePartenaireRepository;
import com.hotel.repositories.InfosPersonnesRepository;
import com.hotel.repositories.OffreRepository;
import com.hotel.repositories.ReservationRepository;

@RestController
public class OffreController {
	
	DateFormat df = new SimpleDateFormat("dd-MM-yyyy");

	Hotel hotel = new Hotel("France", "Montpellier", "Place de la comedie", "HotelMontpellier1000", "HotelComedie", 4 );
	
	@Autowired
	private OffreRepository repository;

	@Autowired
	private ReservationRepository repositoryR;
	
	@Autowired
	private InfosPersonnesRepository repositoryI;
	
	@Autowired
	private AgencePartenaireRepository repositoryA;
	
	private static final String uri = "hotelMontpellier1000/api";
			
	@GetMapping(uri+"/offre")
	public List<Offre> getAllOffres(){
		return repository.findAll();
	}
	
	@GetMapping(uri+"/agencesPartenaires")
	public List<AgencePartenaire> getAllAgences(){
		return repositoryA.findAll();
	}
	
	@GetMapping(uri+"/reservations")
	public List<Reservation> getAllReservations(){
		return repositoryR.findAll();
	}
	
	@GetMapping(uri+"/infosPersonnes")
	public List<InfosPersonnes> getAllInfosPersonnes(){
		return repositoryI.findAll();
	}
	

	@GetMapping(uri+"/offre/count")
	public String count() {
		return String.format("{\"%s\": %d", "count", repository.count());
	}
	
	@GetMapping(uri+"/offre/{id}")
	public Offre getOffreById(@PathVariable long id) throws OffreNotFoundException {
		return repository.findById(id)
				.orElseThrow(() -> new OffreNotFoundException("Impossible de trouver l'offre"));
	}
		
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(uri+"/offre")
	public Offre createOffre(@RequestBody Offre offre) {
		return repository.save(offre);
	}
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(uri+"/consultationOffres")
	public ArrayList<OffreDTO> consultationOffres(@RequestBody ConsultationDTO request) {
		ArrayList<OffreDTO> offres = new ArrayList<>();
		
		if(hotel.getEtoiles()<request.getNbrEtoiles()) {
			return offres;
		}
		
		boolean estPartenaire = false;
		AgencePartenaire agence = new AgencePartenaire();
		
		List<AgencePartenaire> lstAgence = getAllAgences();
		for ( AgencePartenaire a : lstAgence) {
			if(a.getNomAgence().equals(request.getName())) {
				estPartenaire = true;
				agence = a;
			}
		}
		
		if(!estPartenaire) {
			agence = lstAgence.get(0);
		}
		
		
		
		for(Offre o : getAllOffres()) {
			if(o.getNbrLits() >= request.getNbrPersonnes()) {
					if(o.getDateDispo().before(request.getDate())) {
						if(o.getPrix() * (1.00 - agence.getReduc()) <= request.getPrix()) {
					
							OffreDTO offreSend = new OffreDTO(hotel.getRefHotel(), hotel.getNomHotel(), hotel.getRue(), o.getRefOffre(), 
												 o.getPrix() * (1.00 - agence.getReduc()), o.getNbrLits(), hotel.getEtoiles(), o.getDateDispo(), o.getUrlImage());
				
							offres.add(offreSend);
						}
						}
					}
				}
		return offres;
	}
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(uri+"/consultationOffresTrivago")
	public ArrayList<OffreDTO> consultationOffres(@RequestBody ConsultationTrivagoDTO request) {
		ArrayList<OffreDTO> offres = new ArrayList<>();
		
		if(hotel.getEtoiles()<request.getNbrEtoiles()) {
			return offres;
		}
		
		boolean estPartenaire = false;
		AgencePartenaire agence = new AgencePartenaire();
		
		List<AgencePartenaire> lstAgence = getAllAgences();
		System.out.println(lstAgence);
		for ( AgencePartenaire a : lstAgence) {
			if(a.getNomAgence().equals(request.getName())) {
				estPartenaire = true;
				agence = a;
			}
		}
		
		if(!estPartenaire) {
			agence = lstAgence.get(0);
		}
		
		for(Offre o : getAllOffres()) {
			if(o.getNbrLits() >= request.getNbrPersonnes()) {
					if(o.getDateDispo().before(request.getDate())) {				
							OffreDTO offreSend = new OffreDTO(hotel.getRefHotel(), hotel.getNomHotel(), hotel.getRue(), o.getRefOffre(), 
												 o.getPrix() * (1.00 - agence.getReduc()), o.getNbrLits(), hotel.getEtoiles(), o.getDateDispo(), o.getUrlImage());
				
							offres.add(offreSend);
						}
						}
					
				}
		return offres;
	}
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(uri+"/reservationOffre")
	public String reservationOffre(@RequestBody ReservationDTO request) throws ReservationDejaExistante {

		boolean estPartenaire = false;
		boolean clientDejaExistant = false;
		InfosPersonnes ip = new InfosPersonnes();
		AgencePartenaire agence = new AgencePartenaire();
		
		
		// Je dois renvoyer le prix et potentiellement un prix réduit
		for ( AgencePartenaire a : getAllAgences()) {
			if(a.getNomAgence().equals(request.getName())) {
				estPartenaire = true;
				agence = a;
			}
		}
		
		for (InfosPersonnes infos : getAllInfosPersonnes()) {
			if(request.getNom().equals(infos.getNom()) && request.getPrenom().equals(infos.getPrenom())) {
				clientDejaExistant = true;
				ip = infos;
			}
		}
		
		for (Offre o : getAllOffres()) {
			if(o.getRefOffre().equals(request.getRefOffre())) {	
					
				for(Reservation r : getAllReservations()) {
					
					String s1 = new SimpleDateFormat("dd/MM/yyyy").format(request.getDateDebut());
					String s2 = new SimpleDateFormat("dd/MM/yyyy").format(r.getDateDebut());
					
					if( s1.equals(s2) && r.getIdOffre() == o.getId()) {
						throw new ReservationDejaExistante("La chambre est déjà réservée\n");
					};	
				}
			
				long identifiantPersonne; 
				long identifiantAgence;	
				
				if(!clientDejaExistant) {
					InfosPersonnes infosP = new InfosPersonnes(request.getNom(), request.getPrenom());
					InfosPersonnes infosPreturn = repositoryI.save(infosP);
					repositoryI.flush();	
					identifiantPersonne = infosPreturn.getid();
				}
				else {
					identifiantPersonne = ip.getid();
				}
				
				if(!estPartenaire) {
					identifiantAgence = (long) 1;
				}
				else {
					identifiantAgence = agence.getId();
				}
				Reservation res = new Reservation(identifiantAgence, o.getId(), identifiantPersonne, request.getDateDebut(), request.getDateFin(), request.getPrix());
	
				repositoryR.save(res);
				
				updateDateDispo(request.getDateFin(), o.getId());
				return hotel.getRefHotel()+"-"+res.getId();
				}
			}
		return null ;
	}
	
	@PutMapping(uri+"/offre/{id}")
	public Optional<Object> updateDateDispo(@RequestBody Date date, @PathVariable long id) {
		return repository.findById(id)	
				.map(offre -> {
					offre.setDateDispo(date);
					repository.save(offre);
					return offre;
				});
	}
	
	
	
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@DeleteMapping(uri + "/offre/{id}")
	public void deleteOffre (@PathVariable long id) throws OffreNotFoundException{
		Offre offre = repository.findById(id)
				.orElseThrow(() -> new OffreNotFoundException("Impossible de supprimer l'offre car non existante"));
		repository.delete(offre);
	}
	

}
