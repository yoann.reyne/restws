package com.hotel.exceptions;

import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
public class OffreException extends Exception {

	public OffreException() {}
	
	public OffreException(String s) {
		super(s);
	}
	
}
