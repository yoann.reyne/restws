package com.hotel.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hotel.models.Offre;

public interface OffreRepository extends JpaRepository<Offre, Long>{
}
