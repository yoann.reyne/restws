package com.consumer.models;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OffreDTO {

	private String refHotel;
	private String nomHotel;
	private String adresseHotel;
	private String refOffre;
	private String urlImage;
	private double prix;
	private int nbrLits;
	private int nbrEtoiles;
	private Date dateDispo;

	public OffreDTO() {}
	
	
	public OffreDTO(String refHotel, String nomHotel, String adresseHotel, String refOffre, double prix, int nbrLits, int nbrEtoiles, Date dateDispo, String urlImage) {
		super();
		this.refHotel = refHotel;
		this.nomHotel = nomHotel;
		this.adresseHotel = adresseHotel;
		this.refOffre = refOffre;
		this.prix = prix;
		this.nbrLits = nbrLits;
		this.nbrEtoiles = nbrEtoiles;
		this.dateDispo = dateDispo;
		this.urlImage = urlImage;
	}

	

	public String getUrlImage() {
		return urlImage;
	}


	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}


	public String getRefHotel() {
		return refHotel;
	}



	public void setRefHotel(String refHotel) {
		this.refHotel = refHotel;
	}



	public String getNomHotel() {
		return nomHotel;
	}



	public void setNomHotel(String nomHotel) {
		this.nomHotel = nomHotel;
	}



	public String getAdresseHotel() {
		return adresseHotel;
	}



	public void setAdresseHotel(String adresseHotel) {
		this.adresseHotel = adresseHotel;
	}



	public String getRefOffre() {
		return refOffre;
	}



	public void setRefOffre(String refOffre) {
		this.refOffre = refOffre;
	}



	public int getNbrEtoiles() {
		return nbrEtoiles;
	}



	public void setNbrEtoiles(int nbrEtoiles) {
		this.nbrEtoiles = nbrEtoiles;
	}



	public int getNbrLits() {
		return nbrLits;
	}

	public void setNbrLits(int nbrLits) {
		this.nbrLits = nbrLits;
	}

	
	public Date getDateDispo() {
		return dateDispo;
	}

	

	public void setDateDispo(Date dateDispo) {
		this.dateDispo = dateDispo;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix2) {
		this.prix = prix2;
	}


	
}
