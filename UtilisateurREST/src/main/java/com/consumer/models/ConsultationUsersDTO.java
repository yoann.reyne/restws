package com.consumer.models;

import java.util.Date;

public class ConsultationUsersDTO {
	
	private Date date;
	private float prix;
	private int nbrPersonnes;
	private int nbrEtoiles;
	private String ville;
	
	
	public ConsultationUsersDTO(String ville, Date date, float prix, int nbrPersonnes, int nbrEtoiles) {
		super();
		this.ville = ville;
		this.date = date;
		this.prix = prix;
		this.nbrPersonnes = nbrPersonnes;
		this.nbrEtoiles = nbrEtoiles;
	}
	

	public int getNbrEtoiles() {
		return nbrEtoiles;
	}

	public String getVille() {
		return ville;
	}


	public void setVille(String ville) {
		this.ville = ville;
	}


	public void setNbrEtoiles(int nbrEtoiles) {
		this.nbrEtoiles = nbrEtoiles;
	}



	public Date getDate() {
		return date;
	}	
	public void setDate(Date date) {
		this.date = date;
	}
	public float getPrix() {
		return prix;
	}
	public void setPrix(float prix) {
		this.prix = prix;
	}
	public int getNbrPersonnes() {
		return nbrPersonnes;
	}
	public void setNbrPersonnes(int nbrPersonnes) {
		this.nbrPersonnes = nbrPersonnes;
	}
	
	
	
}
