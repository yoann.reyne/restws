package com.consumer.main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import org.springframework.expression.ParseException;
import org.springframework.web.client.RestTemplate;

import com.consumer.models.ConsultationUsersDTO;
import com.consumer.models.OffreDTO;
import com.consumer.models.ReservationUsersDTO;
import com.toedter.calendar.JDateChooser;

public class InterfaceGUI extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JLabel imageChambre;
	private JLabel lblChambre;
	private JTextField nomVille;
	private JTextField prixMaximum;
	private JDateChooser dateA;
	private JDateChooser dateD;
	private ArrayList<OffreDTO> offres = new ArrayList<>();
	
	private DefaultTableModel model;
	JToggleButton tgbltnHistorique;
	JComboBox<String> comboBoxEtoile;
	JComboBox<String> comboBoxAgences;
	JComboBox<String> comboBoxLit;
	ArrayList<String> lstReservations = new ArrayList<>();

	DateFormat df = new SimpleDateFormat("dd-MM-yyyy");

	
	
	String header[] = new String[] {"Lieu", "Nombre de lits", "Prix", "Etoiles", "Identifiant"};
	int row, col;
	private JTable table;
	private JToggleButton tglbtnReservez;
	/**
	 * Launch the application.
	 * @throws RemoteException 
	 * @throws NotBoundException 
	 * @throws MalformedURLException 
	 */
	public static void main(String[] args) throws RemoteException, NotBoundException, MalformedURLException {
			
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfaceGUI frame = new InterfaceGUI();
					frame.setVisible(true);
					frame.getContentPane().setBackground(new Color(192, 192, 192));
					Toolkit toolkit = Toolkit.getDefaultToolkit();  
					Dimension screenSize = toolkit.getScreenSize();  
					int x = (screenSize.width - frame.getWidth()) / 2;  
					int y = (screenSize.height - frame.getHeight()) / 2; 
					frame.setLocation(x, y); 
					JOptionPane.showMessageDialog(null, "Bienvenue sur l'application permettant de consulter et de réserver des hôtels");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws RemoteException 
	 * @throws NotBoundException 
	 * @throws MalformedURLException 
	 */
	public InterfaceGUI() throws RemoteException, NotBoundException, MalformedURLException {
			
		
		this.setTitle("Réservation d'hôtel");
		setType(Type.UTILITY);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100,100,1200,600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		JLabel lblNomVille = new JLabel("Ville souhaitée : ");
		lblNomVille.setBounds(12, 28, 144, 15);
		contentPane.add(lblNomVille);

		
		JLabel lblEtoile = new JLabel("Nombre d'étoiles : ");
		lblEtoile.setBounds(12, 63, 144, 15);
		contentPane.add(lblEtoile);
		
		
		comboBoxEtoile = new JComboBox<>();
		comboBoxEtoile.addItem("");
		for (int i = 1; i < 6 ; i++) {			
			comboBoxEtoile.addItem(String.valueOf(i));
		}
		comboBoxEtoile.setBounds(174, 58, 69, 24);
		contentPane.add(comboBoxEtoile);

		JLabel lblLit = new JLabel("Nombre de lits : ");
		lblLit.setBounds(12, 143, 144, 15);
		contentPane.add(lblLit);
		
		comboBoxLit = new JComboBox<>();
		comboBoxLit.addItem("");
		for (int i = 1; i < 11 ; i++) {			
			comboBoxLit.addItem(String.valueOf(i));
		}
		comboBoxLit.setBounds(174, 140, 69, 24);
		contentPane.add(comboBoxLit);
		
		JLabel lblPrixMax = new JLabel("Prix maximum : ");
		lblPrixMax.setBounds(12, 105, 144, 15);
		contentPane.add(lblPrixMax);
		JLabel lblDateDepart = new JLabel("Date de départ : ");
		lblDateDepart.setBounds(492, 63, 144, 15);
		contentPane.add(lblDateDepart);
		
		JLabel lblDateArrivee = new JLabel("Date d'arrivée : ");
		lblDateArrivee.setBounds(492, 28, 144, 15);
		contentPane.add(lblDateArrivee);
		
		nomVille = new JTextField();
		nomVille.addActionListener(this);
		nomVille.setBounds(174, 26, 143, 19);
		contentPane.add(nomVille);
		nomVille.setColumns(10);
		                                  
		
		tglbtnReservez = new JToggleButton("Réserver");
		tglbtnReservez.addActionListener(this);
		tglbtnReservez.setBounds(150, 182, 123, 25);
		contentPane.add(tglbtnReservez);
		tglbtnReservez.setVisible(false);
		
		JToggleButton tglbtnValidez_1 = new JToggleButton("Validez");
		tglbtnValidez_1.addActionListener(this);
		tglbtnValidez_1.setBounds(31, 182, 86, 25);
		contentPane.add(tglbtnValidez_1);
				
		model = new DefaultTableModel(header,0);
		
		table = new JTable(model) {
			private static final long serialVersionUID = 1L;

	        public boolean isCellEditable(int row, int column) {                
	                return false;               
	        };	
		};
	
		table.getTableHeader().setOpaque(false);
		table.getTableHeader().setForeground(Color.WHITE);
		table.getTableHeader().setBackground(new Color(60, 120, 120));
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setVisible(true);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(141, 307, 894, 219);
		scrollPane.getViewport().setBackground(new Color(210, 210, 210));
		contentPane.add(scrollPane);
		
		prixMaximum = new JTextField();
		prixMaximum.setColumns(10);
		prixMaximum.setBounds(174, 103, 69, 19);
		contentPane.add(prixMaximum);
		
		dateA = new JDateChooser();
		dateA.setBounds(654, 25, 151, 20);
		dateA.setVisible(true);
		contentPane.add(dateA);
		
		dateD = new JDateChooser();
		dateD.setBounds(654, 60, 151, 20);
		dateD.setVisible(true);
		contentPane.add(dateD);		
		
		lblChambre = new JLabel("Image de la chambre :");
		lblChambre.setBounds(492, 163, 200, 15);
		contentPane.add(lblChambre);
		lblChambre.setVisible(false);
		
		imageChambre = new JLabel();
		imageChambre.setBounds(685, 163, 231, 120);
		imageChambre.setSize(120, 120);
		contentPane.add(imageChambre);
		imageChambre.setVisible(false);
		
		comboBoxAgences = new JComboBox<String>();
		comboBoxAgences.addItem("");
		
		comboBoxAgences.addItem("ResaSud");
		comboBoxAgences.addItem("ReserveTonHotel.com");
		
		comboBoxAgences.setBounds(654, 105, 151, 24);
		contentPane.add(comboBoxAgences);
		

		
		JLabel lblAgence = new JLabel("Agence : ");
		lblAgence.setBounds(492, 105, 144, 15);
		contentPane.add(lblAgence);
		
		tgbltnHistorique = new JToggleButton("Historique de vos réservations");
		tgbltnHistorique.setBounds(31, 232, 252, 40);
		tgbltnHistorique.addActionListener(this);
		contentPane.add(tgbltnHistorique);
		tgbltnHistorique.setVisible(false);
		
		if(lstReservations.size()>0) {
			tgbltnHistorique.setVisible(true);
		}
	}

	// Je récupère la valeur des buttons
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand(); 
				
		if(command.equals("Validez")) {
			
			offres.clear();
			((DefaultTableModel)table.getModel()).setNumRows(0); 
			
			String nbLits = String.valueOf(comboBoxLit.getSelectedItem()).trim();	
			String agenceName = String.valueOf(comboBoxAgences.getSelectedItem()).trim();
			model.setRowCount(row);
			String nbEtoiles = String.valueOf(comboBoxEtoile.getSelectedItem()).trim();
			
			
			if(!agenceName.equals("") && !prixMaximum.getText().trim().equals("") && 
					!(dateA.getDate() == null) && !(dateD.getDate() == null) && 
					!nomVille.getText().equals("") && !nbEtoiles.equals("") && !nbLits.equals("")){
			try {
				int prix =  Integer.parseInt(prixMaximum.getText().trim());
				int etoiles = Integer.parseInt(nbEtoiles);
				int lits = Integer.parseInt(nbLits);
				ConsultationUsersDTO consult = new ConsultationUsersDTO(nomVille.getText(),dateA.getDate(), prix, lits, etoiles);
				
				RestTemplate rest = new RestTemplate();

				if(agenceName.equals("ResaSud")) {
					OffreDTO[] response = rest.postForObject("http://localhost:7000/agence/api/consultationOffre", consult, OffreDTO[].class);
					Arrays.asList(response).forEach((n) -> offres.add(n));
				}
				if(agenceName.equals("ReserveTonHotel.com")) {
					OffreDTO[] response = rest.postForObject("http://localhost:7001/agenceRTH/api/consultationOffre", consult, OffreDTO[].class);
					Arrays.asList(response).forEach((n) -> offres.add(n));
				}
				
				for (OffreDTO o : offres) {
					model.addRow(new Object[]
							{ nomVille.getText(), o.getNbrLits(), o.getPrix(), o.getNbrEtoiles(), o.getRefOffre()});
					}
			
			
			
				if(!offres.isEmpty()) {
					tglbtnReservez.setVisible(true);
				}
				else {
					tglbtnReservez.setVisible(false);
				}
				
				table.addMouseListener(new MouseListener() {

					@Override
					public void mouseClicked(MouseEvent e) {
						BufferedImage imgCh = null;
						
						try {
							imgCh = ImageIO.read(new URL("file:"+offres.get(table.getSelectedRow()).getUrlImage()));
						} catch (MalformedURLException e1) {
							e1.printStackTrace();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
						imageChambre.setIcon(new ImageIcon(imgCh));
						lblChambre.setVisible(true);
						imageChambre.setVisible(true);
					}

					@Override
					public void mousePressed(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void mouseReleased(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void mouseEntered(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void mouseExited(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}
				});

			
			
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
			
		}
		else {
			JOptionPane.showMessageDialog(null, "Vous n'avez pas saisi tous les champs.", "Attention", JOptionPane.INFORMATION_MESSAGE);
		}
		}
		
			
		if(command.equals("Historique de vos réservations")) {
			
			String message = lstReservations.get(0);
			
			if(lstReservations.size() == 1) {
				JOptionPane.showMessageDialog(null,message);
			}
			else {
			for (int i = 1; i<lstReservations.size();i++) {
				message = message + ", " + lstReservations.get(i);
			}
				JOptionPane.showMessageDialog(null,message, "Mon historique de réservation", JOptionPane.INFORMATION_MESSAGE);
			}
		
			
			
		}
		
		if(command.equals("Réserver")) {
			if(!table.getSelectionModel().isSelectionEmpty()) {
				
				String identifiant = table.getModel().getValueAt(table.getSelectedRow(), 4).toString();
				double prix = Double.parseDouble(table.getModel().getValueAt(table.getSelectedRow(), 2).toString());
				String agenceName = String.valueOf(comboBoxAgences.getSelectedItem()).trim();
				
				JTextField nom = new JTextField();
				JTextField prenom = new JTextField();
				JTextField numero = new JTextField();
				JTextField cvv = new JTextField();
				JTextField dateExpiration = new JTextField();
				
				
				Object[] message = {
					    "Nom : ", nom,
					    "Prenom : ", prenom,
					    "Numero carte (XXXX-XXXX-XXXX-XXXX) : ", numero,
					    "Date expiration : ", dateExpiration,
					    "CVV : ", cvv
					};
				int option = 0;
				do {
				option = JOptionPane.showConfirmDialog(null, message, "Paiement", JOptionPane.OK_CANCEL_OPTION);
			
				} while (option == 0 && (nom.getText().equals("") || prenom.getText().equals("") || cvv.getText().equals("") || dateExpiration.getText().equals("") || numero.getText().equals("") || !(numero.getText().matches("[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{4}{4}"))));
			
				if(option != 0) {
					JOptionPane.showMessageDialog(null,"Vous venez d'annuler votre réservation");
					return;
				}
				
				String refHotel = "";
				
				for (OffreDTO o : offres) {
					if(o.getRefOffre().equals(identifiant)) {
						refHotel = o.getRefHotel();
					}
				}
				
				ReservationUsersDTO resa;
				String [] parts =  numero.getText().trim().split("-");
				String numeroSend = parts[0] + "-" + parts[1] + "-****-****"; 
				
				RestTemplate rest = new RestTemplate();
				if(agenceName.equals("ResaSud")) {
						resa = new ReservationUsersDTO(identifiant, refHotel, nom.getText(), prenom.getText(), numeroSend, dateExpiration.getText(),Integer.parseInt(cvv.getText().trim()), dateA.getDate(), dateD.getDate(), prix);
						String response = rest.postForObject("http://localhost:7000/agence/api/reservationOffre", resa, String.class);
						lstReservations.add(response);
						
				}
				if(agenceName.equals("ReserveTonHotel.com")) {
					resa = new ReservationUsersDTO(identifiant, refHotel, nom.getText(), prenom.getText(), numeroSend, dateExpiration.getText(),Integer.parseInt(cvv.getText().trim()), dateA.getDate(), dateD.getDate(), prix);
					String response = rest.postForObject("http://localhost:7001/agenceRTH/api/reservationOffre", resa, String.class);
					lstReservations.add(response);
					
				}
				tgbltnHistorique.setVisible(true);
				if(table.getSelectedRow() != -1) {
		        model.removeRow(table.getSelectedRow());		        	
				}
				
	}
}
}

		
	
}