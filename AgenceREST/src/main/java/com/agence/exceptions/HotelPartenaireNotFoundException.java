package com.agence.exceptions;

import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
public class HotelPartenaireNotFoundException extends Exception {

	public HotelPartenaireNotFoundException() {}
	
	public HotelPartenaireNotFoundException(String s) {
		super(s);
	}
	
}
