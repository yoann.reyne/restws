package com.agence.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.agence.models.Hotel;
import com.agence.repositories.HotelPartenaireRepository;


@Configuration
public class AgenceData {

	private Logger logger = LoggerFactory.getLogger(AgenceData.class);

	@Bean
	public CommandLineRunner initDatabase(HotelPartenaireRepository repository) {
		return args -> {
			logger.info("J'enregistre un hotel partenaire : " + repository.save(
					new Hotel("France", "Montpellier", "Place de la comedie", "HotelComedie1000", 3, true)
					));
		};
	}
	
}
