package com.agence.models;

import java.util.Date;

public class ConsultationTrivagoDTO {
	
	private String name;
	private String motdepasse;
	private Date date;
	private int nbrPersonnes;
	private int nbrEtoiles;
	private String ville;
	
	
	public ConsultationTrivagoDTO(String name, String motdepasse, String ville, Date date, int nbrPersonnes, int nbrEtoiles) {
		super();
		this.name = name;
		this.ville = ville;
		this.date = date;
		this.nbrPersonnes = nbrPersonnes;
		this.nbrEtoiles = nbrEtoiles;
	}
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getMotdepasse() {
		return motdepasse;
	}


	public void setMotdepasse(String motdepasse) {
		this.motdepasse = motdepasse;
	}


	public int getNbrEtoiles() {
		return nbrEtoiles;
	}

	public String getVille() {
		return ville;
	}


	public void setVille(String ville) {
		this.ville = ville;
	}


	public void setNbrEtoiles(int nbrEtoiles) {
		this.nbrEtoiles = nbrEtoiles;
	}



	public Date getDate() {
		return date;
	}	
	public void setDate(Date date) {
		this.date = date;
	}
	public int getNbrPersonnes() {
		return nbrPersonnes;
	}
	public void setNbrPersonnes(int nbrPersonnes) {
		this.nbrPersonnes = nbrPersonnes;
	}
	
	
	
}
