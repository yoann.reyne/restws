package com.agence.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.agence.exceptions.HotelPartenaireNotFoundException;
import com.agence.models.Agence;
import com.agence.models.ConsultationDTO;
import com.agence.models.ConsultationTrivagoDTO;
import com.agence.models.ConsultationUsersDTO;
import com.agence.models.ConsultationUsersTrivagoDTO;
import com.agence.models.Hotel;
import com.agence.models.InfosPersonnes;
import com.agence.models.OffreDTO;
import com.agence.models.OffreUsersDTO;
import com.agence.models.Paiement;
import com.agence.models.Reservation;
import com.agence.models.ReservationDTO;
import com.agence.models.ReservationUsersDTO;
import com.agence.repositories.HotelPartenaireRepository;
import com.agence.repositories.InfosPersonnesRepository;
import com.agence.repositories.PaiementRepository;
import com.agence.repositories.ReservationRepository;

@RestController
public class HotelController {
	
	@Autowired
	private HotelPartenaireRepository repository;
		
	@Autowired
	private PaiementRepository repositoryP;
	
	@Autowired
	private ReservationRepository repositoryR;
	
	@Autowired
	private InfosPersonnesRepository repositoryIP;
	
	private Agence agence = new Agence("ResaSud", "ez4d4ezv");
	
	private static final String uri ="agence/api";
	
	@GetMapping(uri+"/HotelPartenaire")
	public List<Hotel> getAllHotelPartenaires(){
		return repository.findAll();
	}
	
	@GetMapping(uri+"/HotelPartenaire/count")
	public String count() {
		return String.format("{\"%s\": %d", "count", repository.count());
	}
	
	@GetMapping(uri+"/HotelPartenaire/{id}")
	public Hotel getHotelPartenaireById(@PathVariable long id) throws HotelPartenaireNotFoundException {
		return repository.findById(id)
				.orElseThrow(() -> new HotelPartenaireNotFoundException("Impossible de trouver l'hôtel"));
	}
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(uri+"/HotelPartenaire")
	public Hotel createHotelPartenairePartenaire(@RequestBody Hotel HotelPartenaire) {
		return repository.save(HotelPartenaire);
	}
	
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@DeleteMapping(uri + "/HotelPartenaire/{id}")
	public void deleteHotelPartenairePartenaire (@PathVariable long id) throws HotelPartenaireNotFoundException{
		Hotel HotelPartenaire = repository.findById(id)
				.orElseThrow(() -> new HotelPartenaireNotFoundException("Impossible de supprimer l'hôtel car non existant"));
		repository.delete(HotelPartenaire);
	}
	
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(uri+"/consultationOffre")
	public ArrayList<OffreDTO> consultationOffres(RestTemplate restTemplate, @RequestBody ConsultationUsersDTO request) throws ParseException, RestClientException, HotelPartenaireNotFoundException {
		
		ConsultationDTO consult = new ConsultationDTO(agence.getNom(), agence.getMotdepasse(), request.getDate(), request.getPrix(), request.getNbrPersonnes());
		ArrayList<OffreDTO> offres = new ArrayList<>();
		
		if(request.getVille().equals("Montpellier")) {
		
			OffreDTO[] responseHotel1 = restTemplate.postForObject("http://localhost:8080/hotelMontpellier1000/api/consultationOffres", consult, OffreDTO[].class);
			Arrays.asList(responseHotel1).forEach(
					(n) -> offres.add(n));
			
			OffreDTO[] responseHotel3 = restTemplate.postForObject("http://localhost:7080/hotelMontpellier1001/api/consultationOffres", consult, OffreDTO[].class);
			Arrays.asList(responseHotel3).forEach(
					(n) -> offres.add(n));
		
		}
		
		if(request.getVille().equals("Nice")) {
			
			OffreDTO[] responseHotel2 = restTemplate.postForObject("http://localhost:8000/hotelNice1000/api/consultationOffres", consult, OffreDTO[].class);
			Arrays.asList(responseHotel2).forEach(
					(n) ->offres.add(n));
			
		}
		
		return offres;
		
	}
	
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(uri+"/consultationOffreTrivago")
	public ArrayList<OffreUsersDTO> consultationOffres(RestTemplate restTemplate, @RequestBody ConsultationUsersTrivagoDTO request) throws ParseException, RestClientException, HotelPartenaireNotFoundException {
		
		ConsultationTrivagoDTO consult = new ConsultationTrivagoDTO(agence.getNom(), agence.getMotdepasse(), request.getVille(),request.getDate(), request.getNbrPersonnes(), request.getNbrEtoiles());
		ArrayList<OffreUsersDTO> offres = new ArrayList<>();
		
		if(request.getVille().equals("Montpellier")) {
			OffreDTO[] responseHotel1 = restTemplate.postForObject("http://localhost:8080/hotelMontpellier1000/api/consultationOffresTrivago", consult, OffreDTO[].class);
			Arrays.asList(responseHotel1).forEach(
					(n) -> offres.add(new OffreUsersDTO(n.getRefHotel(), agence.getNom(), n.getNomHotel(), 
							n.getAdresseHotel(), n.getRefOffre(), n.getPrix(), n.getNbrLits(), n.getNbrEtoiles(), n.getDateDispo(), n.getUrlImage())));
			
			OffreDTO[] responseHotel3 = restTemplate.postForObject("http://localhost:7080/hotelMontpellier1001/api/consultationOffresTrivago", consult, OffreDTO[].class);
			Arrays.asList(responseHotel3).forEach(
					(n) -> offres.add(new OffreUsersDTO(n.getRefHotel(), agence.getNom(), n.getNomHotel(), 
							n.getAdresseHotel(), n.getRefOffre(), n.getPrix(), n.getNbrLits(), n.getNbrEtoiles(), n.getDateDispo(), n.getUrlImage())));
		
		}
		
		if(request.getVille().equals("Nice")) {
			
			OffreDTO[] responseHotel2 = restTemplate.postForObject("http://localhost:8000/hotelNice1000/api/consultationOffresTrivago", consult, OffreDTO[].class);
			Arrays.asList(responseHotel2).forEach(
					(n) -> offres.add(new OffreUsersDTO(n.getRefHotel(), agence.getNom(), n.getNomHotel(), 
							n.getAdresseHotel(), n.getRefOffre(), n.getPrix(), n.getNbrLits(), n.getNbrEtoiles(), n.getDateDispo(), n.getUrlImage())));
		
			
		}
		
		return offres;
		
	}
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(uri+"/reservationOffre")
	public String reservationOffres(RestTemplate restTemplate, @RequestBody ReservationUsersDTO request) throws ParseException {

		ReservationDTO reservation = new ReservationDTO(agence.getNom(), agence.getMotdepasse(), request.getRefOffre(), request.getLastName(), request.getFirstName(), request.getDateDebut(), request.getDateFin(), request.getPrix());
		String result = "";
		if(request.getRefHotel().equals("HotelMontpellier1000")) {
			result = restTemplate.postForObject("http://localhost:8080/hotelMontpellier1000/api/reservationOffre", reservation, String.class);
		}
		if(request.getRefHotel().equals("HotelMontpellier1001")) {
			result = restTemplate.postForObject("http://localhost:7080/hotelMontpellier1001/api/reservationOffre", reservation, String.class);
		}
		if(request.getRefHotel().equals("HotelNice1000")) {
			result = restTemplate.postForObject("http://localhost:8000/hotelNice1000/api/reservationOffre", reservation, String.class);
		}
		Paiement p = new Paiement(request.getNumeroCarte(), request.getDateExpiration(), request.getCvv());
		Paiement pReturn = repositoryP.save(p);
		repositoryP.flush();
		
		InfosPersonnes ip = new InfosPersonnes(request.getLastName(), request.getFirstName());
		InfosPersonnes ipReturn = repositoryIP.save(ip);
		repositoryIP.flush();
		
		Reservation res = new Reservation(request.getRefHotel(), pReturn.getId(), request.getRefOffre(), ipReturn.getid(), request.getDateDebut(), request.getDateFin());
		repositoryR.save(res);
		
		return result; 
	}
	
}
