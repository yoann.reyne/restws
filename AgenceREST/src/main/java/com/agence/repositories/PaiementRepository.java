package com.agence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.agence.models.Paiement;

public interface PaiementRepository extends JpaRepository<Paiement, Long>{
}
