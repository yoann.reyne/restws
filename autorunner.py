import os
import subprocess
import time

def compileProjet():
    subprocess.call(
        ["make", "compile"]
    )

def runProjet(nbr):
    command="make pHotel1"
    os.system("gnome-terminal -- bash -c 'bash -c \""+command+";bash\"'")

    command="make pHotel2"
    os.system("gnome-terminal -- bash -c 'bash -c \""+command+";bash\"'")

    command="make pHotel3"
    os.system("gnome-terminal -- bash -c 'bash -c \""+command+";bash\"'")

    command="make pAgence1"
    os.system("gnome-terminal -- bash -c 'bash -c \""+command+";bash\"'")

    command="make pAgence2"
    os.system("gnome-terminal -- bash -c 'bash -c \""+command+";bash\"'")

    time.sleep(3)
    if(nbr == 2):
        command="make trivago"
        os.system("gnome-terminal -- bash -c 'bash -c \""+command+";bash\"'")
    else:
        command="make utilisateur"
        os.system("gnome-terminal -- bash -c 'bash -c \""+command+";bash\"'")

print("\nBienvenue sur mon projet basé sur l'architecture REST")

interface = 0

if(interface != 1 or interface != 2):

    print("\nChoisissez le lancement désiré : \n")
    print("1 - Lancement de l'inferface Utilisateur")
    print("2 - Lancement de l'inferface Trivago")
    interface=int(input("\nVeuillez sélectionner une interface à éxecuter : "))

    print("\nJe vais procéder à la compilation des programmes\n")
    time.sleep(3)
    compileProjet()
    time.sleep(2)
    print("Je vais maintenant procéder au lancement des serveurs mais également à l'ouverture de l'interface")
    runProjet(interface)