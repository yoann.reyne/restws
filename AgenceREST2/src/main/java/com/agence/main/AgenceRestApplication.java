package com.agence.main;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;

@EntityScan(basePackages = {
		"com.agence.models"
})

@EnableJpaRepositories(basePackages = {
		"com.agence.repositories"	
})
@SpringBootApplication(scanBasePackages = {
		"com.agence.data",
		"com.agence.exceptions",
		"com.agence.controller"
})

public class AgenceRestApplication {
	


	
	public static void main(String[] args) {
		SpringApplication.run(AgenceRestApplication.class, args);
	}
	
	@Bean 
	public RestTemplate generateRestTemplate(RestTemplateBuilder builder) {
		return builder.build(); // Construction du proxy
	}
	
	
	

}
	

