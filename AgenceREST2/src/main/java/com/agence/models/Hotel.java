package com.agence.models;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Hotel {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "pays")
	private String pays;
	
	@Column(name = "ville")
	private String ville;
	
	@Column(name = "rue")
	private String rue;
	
	@Column(name = "refHotel")
	private String refHotel;
	
	@Column(name = "etoiles")
	private int etoiles;
	
	@Column(name = "estPartenaire")
	private boolean estPartenaire;

	public Hotel() {}
	
	public Hotel(String pays, String ville, String rue, String refHotel, int etoiles, boolean estPartenaire) {
		this.pays = pays;
		this.ville = ville;
		this.rue = rue;
		this.refHotel = refHotel;
		this.etoiles = etoiles;
		this.estPartenaire = estPartenaire;
	}
	
	
	public boolean isEstPartenaire() {
		return estPartenaire;
	}

	public void setEstPartenaire(boolean estPartenaire) {
		this.estPartenaire = estPartenaire;
	}

	public String getRefHotel() {
		return refHotel;
	}

	public void setRefHotel(String refHotel) {
		this.refHotel = refHotel;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public String getPays() {
		return pays;
	}

	
	public void setPays(String pays) {
		this.pays = pays;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public int getEtoiles() {
		return etoiles;
	}

	public void setEtoiles(int etoiles) {
		this.etoiles = etoiles;
	}

}
