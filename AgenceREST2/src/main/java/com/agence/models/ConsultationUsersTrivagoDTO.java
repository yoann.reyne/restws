package com.agence.models;

import java.util.Date;

public class ConsultationUsersTrivagoDTO {
	
	private Date date;
	private int nbrPersonnes;
	private int nbrEtoiles;
	private String ville;
	
	
	public ConsultationUsersTrivagoDTO(String ville, Date date, int nbrPersonnes, int nbrEtoiles) {
		super();
		this.ville = ville;
		this.date = date;
		this.nbrPersonnes = nbrPersonnes;
		this.nbrEtoiles = nbrEtoiles;
	}
	

	public int getNbrEtoiles() {
		return nbrEtoiles;
	}

	public String getVille() {
		return ville;
	}


	public void setVille(String ville) {
		this.ville = ville;
	}


	public void setNbrEtoiles(int nbrEtoiles) {
		this.nbrEtoiles = nbrEtoiles;
	}



	public Date getDate() {
		return date;
	}	
	public void setDate(Date date) {
		this.date = date;
	}

	public int getNbrPersonnes() {
		return nbrPersonnes;
	}
	public void setNbrPersonnes(int nbrPersonnes) {
		this.nbrPersonnes = nbrPersonnes;
	}
	
	
	
}
