package com.agence.models;

import java.util.Date;

public class ReservationUsersDTO {

	private String refOffre;
	private String refHotel;
	private String lastName;
	private String firstName;
	private String numeroCarte;
	private String dateExpiration;
	private int cvv;
	private Date dateDebut;
	private Date dateFin;
	private double prix;	
	
	public ReservationUsersDTO(String refOffre, String refHotel, String lastName, String firstName, String numeroCarte,
			String dateExpiration, int cvv, Date dateDebut, Date dateFin, double prix) {
		super();
		this.refOffre = refOffre;
		this.refHotel = refHotel;
		this.lastName = lastName;
		this.firstName = firstName;
		this.numeroCarte = numeroCarte;
		this.dateExpiration = dateExpiration;
		this.cvv = cvv;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.prix = prix;
	}


	public String getRefOffre() {
		return refOffre;
	}


	public void setRefOffre(String refOffre) {
		this.refOffre = refOffre;
	}


	public String getRefHotel() {
		return refHotel;
	}


	public void setRefHotel(String refHotel) {
		this.refHotel = refHotel;
	}


	public double getPrix() {
		return prix;
	}


	public void setPrix(double prix) {
		this.prix = prix;
	}

	public String getNumeroCarte() {
		return numeroCarte;
	}

	public void setNumeroCarte(String numeroCarte) {
		this.numeroCarte = numeroCarte;
	}

	public String getDateExpiration() {
		return dateExpiration;
	}

	public void setDateExpiration(String dateExpiration) {
		this.dateExpiration = dateExpiration;
	}

	public int getCvv() {
		return cvv;
	}

	public void setCvv(int cvv) {
		this.cvv = cvv;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public Date getDateDebut() {
		return dateDebut;
	}
	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}
	public Date getDateFin() {
		return dateFin;
	}
	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
}
