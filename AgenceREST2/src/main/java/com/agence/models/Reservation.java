package com.agence.models;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Reservation {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "refHotel")
	private String refHotel;

	@Column(name = "idPaiement")
	private long idPaiement;

	@Column(name = "refOffre")
	private String refOffre;

	@Column(name = "idInfosClient")
	private long idInfosClient;

	@Column(name = "dateDebut")
	private Date dateDebut;
	
	@Column(name = "dateFin")
	private Date dateFin;
	
	public Reservation() {}	
	
	public Reservation(String refHotel, long idPaiement, String refOffre, long idInfosClient, Date dateDebut, Date dateFin) {
		super();
		this.refOffre = refOffre;
		this.idPaiement = idPaiement;
		this.refHotel = refHotel;
		this.idInfosClient = idInfosClient;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
	}


	public String getRefHotel() {
		return refHotel;
	}

	public void setRefHotel(String refHotel) {
		this.refHotel = refHotel;
	}

	public String getRefOffre() {
		return refOffre;
	}

	public void setRefOffre(String refOffre) {
		this.refOffre = refOffre;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public long getIdPaiement() {
		return idPaiement;
	}

	public void setIdPaiement(long idPaiement) {
		this.idPaiement = idPaiement;
	}

	public long getIdInfosClient() {
		return idInfosClient;
	}

	public void setIdInfosClient(long idInfosClient) {
		this.idInfosClient = idInfosClient;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}
	


}