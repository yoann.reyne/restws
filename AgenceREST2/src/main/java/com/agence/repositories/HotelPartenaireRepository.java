package com.agence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.agence.models.Hotel;

public interface HotelPartenaireRepository extends JpaRepository<Hotel, Long>{
}
