package com.agence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.agence.models.InfosPersonnes;

public interface InfosPersonnesRepository extends JpaRepository<InfosPersonnes, Long>{
}
