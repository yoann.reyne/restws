package com.agence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.agence.models.Reservation;

public interface ReservationRepository extends JpaRepository<Reservation, Long>{
}
