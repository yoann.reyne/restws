# HAI704I TP2 - Web service REST

TP réalisé par :
- REYNE Yoann

## Pré-requis

- Système unix (recommandé)
- Make
- Maven
- Java JRE
- Python 3.8 ou ultérieure

## Installation

- Récupérer une archive (recommandé)

### Vous avez deux posibilités

Première posibilité (plus rapide mais uniquement sur Linux) :
    
- Exécuter la commande Python suivante et choissisez l'interface que vous souhaitez :
      
```bash
python3 autorunner.py
```
    
Deuxième posibilité :

- Ouvrez l'ensemble des projets sur votre IDE préféré et exécuté les un par un.

## Utilisation

### Interface Trivago

Le but de cette interface est de comparer le tarif des chambres qui peut varier suivant l'agence.

- Remplir les critères désirés
    - Ville souhaitée
    - Dates de séjour
    - Nombre d'étoiles de l'hôtel
    - Nombre de personnes à héberger

- (Villes disponibles : )
    - Montpellier
    - Nice

- Consulter les offres répondant à ses critères
- Des réductions peuvent apparaître suivant l'agence
- Effectuer une réservation parmi les offres obtenues
- Accéder à l'historique des réservations


### Interface Utilisateur

Cette interface va fournir un moyen simple de consultation et de réservation de chambre(s) d'un ou plusieurs hôtels. 

- Remplir les critères désirés
    - Ville souhaitée
    - Prix maximum
    - Dates de séjour
    - Nombre d'étoiles de l'hôtel
    - Nombre de personnes à héberger

- (Villes disponibles : )
    - Montpellier
    - Nice

- Consulter les offres répondant à ses critères
- Effectuer une réservation parmi les offres obtenues
- Accéder à l'historique des réservations
