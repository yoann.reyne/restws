package com.hotel.exceptions;

import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
public class OffreNotFoundException extends Exception {

	public OffreNotFoundException() {}
	
	public OffreNotFoundException(String s) {
		super(s);
	}
	
}
