package com.hotel.models;

import java.util.Date;

public class ConsultationDTO {

	private String name;
	private String motdepasse;
	private Date date;
	private float prix;
	private int nbrPersonnes;
	private int nbrEtoiles;
	
	public ConsultationDTO(String name, String motdepasse, Date date, float prix, int nbrPersonnes, int nbrEtoiles) {
		super();
		this.name = name;
		this.motdepasse = motdepasse;
		this.date = date;
		this.prix = prix;
		this.nbrPersonnes = nbrPersonnes;
		this.nbrEtoiles = nbrEtoiles;
	}
	
	
	
	public int getNbrEtoiles() {
		return nbrEtoiles;
	}



	public void setNbrEtoiles(int nbrEtoiles) {
		this.nbrEtoiles = nbrEtoiles;
	}



	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public String getMotdepasse() {
		return motdepasse;
	}
	public void setMotdepasse(String motdepasse) {
		this.motdepasse = motdepasse;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public float getPrix() {
		return prix;
	}
	public void setPrix(float prix) {
		this.prix = prix;
	}
	public int getNbrPersonnes() {
		return nbrPersonnes;
	}
	public void setNbrPersonnes(int nbrPersonnes) {
		this.nbrPersonnes = nbrPersonnes;
	}
	
	
	
}
