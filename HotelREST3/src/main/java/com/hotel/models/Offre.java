package com.hotel.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Offre {	
	
	// Identifiant de la chambre généré automatiquement
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "prix")
	private double prix;

	@Column(name = "nbrLits")
	private int nbrLits;
	
	@Column(name = "refOffre")
	private String refOffre;
	
	@Column(name = "DateDispo")
	private Date dateDispo;
	
	@Column(name = "urlImage")
	private String urlImage;
	
	public Offre() {}
	

	public Offre(double prix, int nbrLits, String refOffre, Date dateDispo, String urlImage) {
		super();
		this.prix = prix;
		this.nbrLits = nbrLits;
		this.refOffre = refOffre;
		this.dateDispo = dateDispo;
		this.urlImage = urlImage;
	}



	public String getUrlImage() {
		return urlImage;
	}


	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}


	public String getRefOffre() {
		return refOffre;
	}



	public void setRefOffre(String refOffre) {
		this.refOffre = refOffre;
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getNbrLits() {
		return nbrLits;
	}

	public void setNbrLits(int nbrLits) {
		this.nbrLits = nbrLits;
	}

	
	public Date getDateDispo() {
		return dateDispo;
	}

	

	public void setDateDispo(Date dateDispo) {
		this.dateDispo = dateDispo;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix2) {
		this.prix = prix2;
	}


	
}
