package com.hotel.models;

import java.util.Date;

public class ReservationDTO {

	private String name;
	private String motdepasse;
	private String refOffre;
	private String nom;
	private String prenom;
	private Date dateDebut;
	private Date dateFin;
	private double prix;
	
	public ReservationDTO(String name, String motdepasse, String refOffre, String nom, String prenom, Date dateDebut, Date dateFin, double prix) {
		super();
		this.name = name;
		this.motdepasse = motdepasse;
		this.nom = nom;
		this.prenom = prenom;
		this.refOffre = refOffre;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.prix = prix;
	}
	
	
	public double getPrix() {
		return prix;
	}


	public void setPrix(double prix) {
		this.prix = prix;
	}


	public Date getDateDebut() {
		return dateDebut;
	}
	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}
	public Date getDateFin() {
		return dateFin;
	}
	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMotdepasse() {
		return motdepasse;
	}
	public void setMotdepasse(String motdepasse) {
		this.motdepasse = motdepasse;
	}
	

	public String getRefOffre() {
		return refOffre;
	}


	public void setRefOffre(String refOffre) {
		this.refOffre = refOffre;
	}


	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	
	
}
