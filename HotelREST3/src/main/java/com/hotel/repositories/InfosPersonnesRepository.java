package com.hotel.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hotel.models.InfosPersonnes;

public interface InfosPersonnesRepository extends JpaRepository<InfosPersonnes, Long>{
}
