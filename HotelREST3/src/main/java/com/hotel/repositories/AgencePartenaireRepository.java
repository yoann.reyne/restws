package com.hotel.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hotel.models.AgencePartenaire;

public interface AgencePartenaireRepository extends JpaRepository<AgencePartenaire, Long>{
	
}
